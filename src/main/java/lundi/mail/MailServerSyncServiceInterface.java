
package main.java.mail;

import java.util.Date;
import main.java.mail.Mail;

/**
 * service qui va s'assurer que les mails serveur et local sont au même niveau
 */
public interface MailServerSyncServiceInterface {

  /**
   * récupère tous les mails du serveur après une certaine date
   * @param mail
   * @param date
   * @return
   */
  public Mail[] getAllMailsFromServeurAfterDate(Mail mail, Date date);

  /**
   * récupère la date du mail le plus récent sur le stockage local
   * @return
   */
  public Date getDateOfMostRecentMailOnLocal();
  
}
