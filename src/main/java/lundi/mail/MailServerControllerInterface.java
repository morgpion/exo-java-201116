
package main.java.mail;

import main.java.mail.Mail;
/**
 * controller de l'interaction avec le serveur de mail
 */
public interface MailServerControllerInterface {

  /**
   * envoie un mail au service du serveur mail
   */
  public void sendMailToServeur(Mail mail);
  
}
