
package main.java.mail;

import main.java.mail.Mail;
/**
 * controller de l'interaction avec le stockage mail local
 */
public interface MailLocalControllerInterface {

  /** 
   * prend un mail et l'envoie au service des mails locaux
   * @param mail
   */
  public void addMailToLocal(Mail mail);
  
}
