
package main.java.mail;

import main.java.mail.Mail;

public interface MailLocalServerInterface {

  /**
   * reçoit un mail et le parse en string, puis le retourn
   * @param mail
   * @return
   */
  public String mailParser(Mail mail);
 
  /**
   * prend un mail, le parse et utilise le dao pour l'ajouter au stockage
   */
  public void addmailToStockage(Mail mail);

}
