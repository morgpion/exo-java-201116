
package main.java.mail;

/**
 * Modèle d'un utilisateur qui va envoyer des mails (donc pas un destinataire)
 */
public class User {
  
  private Integer id;
  private String name;
  private String address;
  private String password;

  User() {
    
  }
  
  public User(String name, String address, String password) {
    this.name = name;
    this.address = address;
    this.password = password;
  }
  
  //  getters et setters
  
}
