
package mail.java.mail;

import ja.io.File;

/**
 * s'occupe de lire le fichier txt qui contient les mails sur le local
 * (dao)
 * doit extend un BufferReader
 */
public interface TxtMailReaderInterface {
  
  /**
   * ouvre le flux avec le fichier selon le chemin demandé
   * @return
   */
  public File loadFile(String filepath);

  /**
   * ferme le flux
   * @param file
   */
  public void closeFile(File file);

  /**
   * crée un fichier txt si besoin et le retourne
   * @return
   */
  public File createFile();

  /**
   * lit tout le fichier et le renvoie en liste de strins
   * @return
   */
  public String[] readAllLines();

  /**
   * lit une ligne en particulier et la renvoie en String
   * @param id
   * @return
   */
  public String readOneLine(Integer id);

  /**
   * lit la dernière ligne du fichier
   * @return
   */
  public String readLastLine();

}
