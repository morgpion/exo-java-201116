
package main.java.mail;

/**
 * s'occupe d'écrire les nouveaux mails dans le fichier qui contient les mails
 * doit extend un BufferWriter
 */
public interface TxtMailWriterInterface {

  /**
   * ajoute une ligne dans le fichier
   * @param newLine
   */
  public void addLine(String newLine);
  
}
