
package main.java.mail;

import main.java.Mail;
import main.java.User;

/**
 * gère la connexion avec le serveur mail
 */
public interface MailServerServiceInterface {

  /**
   * se connecte au serveur de mail
   */
  public void connectToMailServer(User user);

  /**
   * récupère tous les nouveaux mails du serveur
   * @return
   */
  public Mail[] getAllNewMailsFromServer();

  /**
   * envoie un mail au serveur
   */
  public void sendMailToServer(Mail mail);
  
}
